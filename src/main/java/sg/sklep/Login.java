package sg.sklep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Boolean loginFailed = getSessionAttribute(req,"loginAttemptFailed");

        if(loginFailed != null) {
            req.setAttribute("loginAttemptFailed", loginFailed);
        }
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logLoginAttempts(req, resp);
        authenticateUser(req, resp);
    }

    private void logLoginAttempts(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        ArrayList<String> loginAttempts = new ArrayList<>();
        String key = "loginAttempts";
        if(getSessionAttribute(req, key) != null) {
            loginAttempts = (ArrayList<String>) getSessionAttribute(req, key);
        }
        String msg = "Logowanie korzystajac z danych: " + login + "/" + password;
        loginAttempts.add(msg);
        req.getSession().setAttribute(key, loginAttempts);

        PrintWriter out = resp.getWriter();
        for (String entry : loginAttempts) {
            out.println(entry);
        }
    }

    private void authenticateUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Boolean isAuthenticated = getSessionAttribute(req, "isAuthenticated");

        if(Boolean.TRUE.equals(isAuthenticated)){
            return;
        }

        boolean passwordCorrect = isUserAndPassowrdAMatch(req);
        if(passwordCorrect == false){
            req.getSession().setAttribute("loginAttemptFailed", true);
            resp.sendRedirect("login");
        }
        else{
            req.getSession().setAttribute("isAuthenticated", Boolean.TRUE);
            String username = req.getParameter("login");
            req.getSession().setAttribute("username", username);
            req.getSession().removeAttribute("loginAttempts");
        }
    }

    private boolean isUserAndPassowrdAMatch(HttpServletRequest req) {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        return "secret".equalsIgnoreCase(password);
    }

    private <T> T getSessionAttribute(HttpServletRequest req, String attributeName) {
        return (T)req.getSession().getAttribute(attributeName);
    }
}
