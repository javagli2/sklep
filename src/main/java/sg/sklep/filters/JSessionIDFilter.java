package sg.sklep.filters;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class JSessionIDFilter implements javax.servlet.Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        doFilter((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, filterChain);

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void doFilter(HttpServletRequest servletRequest, HttpServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = servletRequest.getSession();
        if (servletRequest.getParameter("JSESSIONID") != null) {
            Cookie userCookie = new Cookie("JSESSIONID", servletRequest.getParameter("JSESSIONID"));
            userCookie.setDomain("localhost");
            userCookie.setPath("/jsp-00");
            userCookie.setHttpOnly(true);
            servletResponse.addCookie(userCookie);
        } else {
            String sessionId = session.getId();
            Cookie userCookie = new Cookie("JSESSIONID", sessionId);
            userCookie.setDomain("localhost");
            userCookie.setPath("/jsp-00");
            userCookie.setHttpOnly(true);
            servletResponse.addCookie(userCookie);
        }
    }

    @Override
    public void destroy() {

    }
}
