package sg.sklep.filters;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

public class Filter2 implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        PrintWriter writer = servletResponse.getWriter();
        writer.print("<b>Filter2</b>");
        filterChain.doFilter(servletRequest, servletResponse);
        writer.print("<b>Filter2</b>");
    }

    @Override
    public void destroy() {

    }
}
