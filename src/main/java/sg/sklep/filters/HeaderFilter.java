package sg.sklep.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class HeaderFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.getRequestDispatcher("header.jsp").include(servletRequest, servletResponse);

        Boolean isAuthenticated = getSessionAttribute((HttpServletRequest) servletRequest,"loginAttemptFailed");
        servletRequest.setAttribute("isAuthenticated", isAuthenticated);

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }

    private <T> T getSessionAttribute(HttpServletRequest req, String attributeName) {
        return (T)req.getSession().getAttribute(attributeName);
    }
}
