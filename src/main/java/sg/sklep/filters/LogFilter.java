package sg.sklep.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class LogFilter implements javax.servlet.Filter {

    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        String ipAddress = request.getRemoteAddr();
        System.out.println("IP " + ipAddress + ", Time " + new Date().toString());
        if(warunkiDoZablokowania((HttpServletRequest) request, (HttpServletResponse)response)) {
            request.getRequestDispatcher("kontozablokowane.jsp").forward(request, response);
        }
        chain.doFilter(request, response);
    }

    private boolean warunkiDoZablokowania(HttpServletRequest req, HttpServletResponse response) throws IOException {
        ArrayList<String> loginAttempts = new ArrayList<>();
        String key = "loginAttempts";
        if(getSessionAttribute(req, key) != null) {
            loginAttempts = (ArrayList<String>) getSessionAttribute(req, key);
        }

        if(loginAttempts.size() >= 3){
            return true;
        }
        return false;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void destroy() {
    }

    private <T> T getSessionAttribute(HttpServletRequest req, String attributeName) {
        return (T)req.getSession().getAttribute(attributeName);
    }
}