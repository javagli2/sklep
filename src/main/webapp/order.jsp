<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 22.08.2020
  Time: 16:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order</title>
</head>
<body>
<h2>Zamówienie</h2>
<form action="order" method="POST">
    <label for="product">Wybierz produkt: </label>
    <select name="product" id="product">
        <option value="product1">Produkt 1</option>
        <option value="product2">Produkt 2</option>
        <option value="product3">Produkt 3</option>
    </select>
    <p>
        Ilość: <input name="amount" type="text"/>
    </p>
    <p>
        Dane kontaktowe: <br>
        Imię: <input name="firstName" type="text"/> <br>
        Nazwisko: <input name="lastName" type="text"/> <br>
        Telefon: <input name="phoneNumber" type="text"/> <br>
        E-mail: <input name="email" type="text"/>
    </p>
    <p>
        Adres dostawy: <br>
        Ulica, numer domu, numer mieszkania: <input name="address" type="text"/> <br>
        Kod pocztowy: <input name="postCode" type="text"/> <br>
        Miasto: <input name="city" type="text"/>
    </p>
    <p>
        <input value="Zamów" type="submit"/>
    </p>
</form>
</body>
</html>
