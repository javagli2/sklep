<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false"%>

    <c:if test = "${isAuthenticated}">
        <p>${username} <a href="logout">Wyloguj</a></p>
    </c:if>
<hr>