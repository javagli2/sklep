<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false"%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
    <h2>Panel logowania</h2>
Wartość zmiennej: ${loginAttemptFailed}
    <c:if test = "${loginAttemptFailed}">
        User and password do not match, try again!
    </c:if>
    <form action="login" method="POST">
        Login: <input name="login" type="text"/><br/>
        Haslo: <input name="password" type="password"/><br/>
        <input value="Zaloguj" type="submit"/>
    </form>
</body>
</html>
